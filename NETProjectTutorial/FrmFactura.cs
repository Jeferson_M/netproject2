﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmFactura : Form
    {

        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        private double subtotal;
        private double iva;
        private double total;

        public FrmFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }

        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsSistema.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            cmbProductos.DataSource = dsSistema.Tables["Producto"];
            cmbProductos.DisplayMember = "SKU";
            cmbProductos.ValueMember = "Id";

            dsSistema.Tables["ProductoFactura"].Rows.Clear();
            bsProductoFactura.DataSource = dsSistema.Tables["ProductoFactura"];
            dgvProductoFactura.DataSource = bsProductoFactura;


        }

        private void cmbProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow drProduto = ((DataRowView)cmbProductos.SelectedItem).Row;
            txtCantidad.Text = drProduto["Cantidad"].ToString();
            txtPrecio.Text = drProduto["Precio"].ToString();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drProduto = ((DataRowView)cmbProductos.SelectedItem).Row;
                DataRow drProdutoFactura = dsSistema.Tables["ProdutoFactura"].NewRow();
                drProdutoFactura["Id"] = drProduto["Id"];
                drProdutoFactura["SKU"] = drProduto["SKU"];
                drProdutoFactura["Nombre"] = drProduto["Nombre"];
                drProdutoFactura["Cantidad"] = 1;
                drProdutoFactura["Precio"] = drProduto["Precio"];
                dsSistema.Tables["PrductoFactura"].Rows.Add(drProdutoFactura);
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "Error, Producto existente", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void dgvProductoFactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
